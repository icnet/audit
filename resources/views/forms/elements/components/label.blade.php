{{--
Labels for form elements
Options:
    id = element id
    label = element label
    required = required (boolean)
--}}
<label for="{{$id}}">
    {{$label}}
    @if (isset($required) && $required === true)
        <span class="text-danger">*</span>
    @endif
</label>
