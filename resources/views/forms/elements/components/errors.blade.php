{{--
Form errors
Options:
    name = name of form field
--}}
@if ($errors->get($name))
<ul class="parsley-errors-list filled">
    @foreach ($errors->get($name) as $error)
        <li class="parsley-required">{{$error}}</li>
    @endforeach
</ul>
@endif
