{{--
Text form group
Options:
    id = element identifier
    name = name of form element
    label = element label
    required = required (boolean)
    value = set value (optional)
    maxlength = max input length (optional)
--}}
<div class="form-group">
    @include('forms.elements.components.label', [ 'id' => $id, 'label' => $label, 'name' => $name, 'required' => $required ?? false ] )
    <input type="text" class="form-control" name="{{$name}}" value="{{old($name, $value ?? '')}}" maxlength="{{$maxlength ?? ''}}" id="{{$id}}">
    @include('forms.elements.components.errors', [ 'name' => $name ])
</div>
