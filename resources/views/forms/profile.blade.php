<form action="{{route('profile.update')}}" method="post" id="frm-profile">
    @csrf
    @include('forms.elements.text', [
        'id' => 'frm-profile-first-name',
        'name' => 'first_name',
        'label' => __('First name'),
        'required' => true,
        'value' => $profile->first_name ?? null
    ])
    @include('forms.elements.text', [
        'id' => 'frm-profile-last-name',
        'name' => 'last_name',
        'label' => __('Last name'),
        'required' => true,
        'value' => $profile->last_name ?? null
    ])
    @include('forms.elements.text', [
        'id' => 'frm-profile-email',
        'name' => 'email',
        'label' => __('Email'),
        'required' => true,
        'value' => $profile->email ?? null
    ])

    <input type="submit" class="btn btn-primary" value="{{__('Save')}}">

</form>
