<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Outbreaks') }}@hasSection('title'): @yield('title')@endif</title>

    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <link href="{{mix('/css/app.css')}}" rel="stylesheet">

    <!-- Scripts -->
    <!-- CSRF Token -->
    <script>
        window.Laravel = {"csrfToken": "<?= csrf_token() ?>"};
    </script>
  </head>
  <body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

      <!-- Top Bar Start -->
      <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
          <div class="text-center">
            <a href="" class="logo">
              <i class="mdi mdi-account"></i>
              <span>{{ config('app.name', __('Profile')) }}</span>
            </a>
          </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <nav class="navbar-custom">

          <ul class="list-inline float-right mb-0">

            <li class="list-inline-item dropdown notification-list">
              <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                 aria-haspopup="false" aria-expanded="false">
                <i class="mdi mdi-help-circle noti-icon"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg" aria-labelledby="About">
                <!-- item-->
                <div class="dropdown-item noti-title">
                  <h5 class="font-16">{{__('Help')}}</h5>
                </div>

                <!-- item-->
                <a href="https://icnetknowledgebase.helpdocsonline.com/home" class="dropdown-item notify-item">
                  <i class="mdi mdi-book-open-variant text-info"></i>
                  <span>{{__('Knowledge base')}}</span>
                </a>
                <!-- item-->
                <a href="https://devcampus.jira.com/servicedesk/customer/portal/6" class="dropdown-item notify-item">
                  <i class="mdi mdi-headset text-info"></i>
                  <span>{{__('Service desk')}}</span>
                </a>
                <!-- item-->
                <a href="mailto:helpdesk@icnetplc.com" class="dropdown-item notify-item">
                  <i class="mdi mdi-email text-info"></i>
                  <span>{{__('Email')}}</span>
                </a>

              </div>
            </li>

            <li class="list-inline-item dropdown notification-list">
              <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                 aria-haspopup="false" aria-expanded="false">
                <img src="@include('helpers.avatar')" alt="user" class="rounded-circle">
              </a>
              <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                <!-- item-->
                <div class="dropdown-item noti-title">
                  <h5 class="text-overflow"><small>{{ Auth::user()->name }}</small> </h5>
                </div>

                <!-- item-->
                <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                  <i class="mdi mdi-logout"></i> <span>Logout</span>
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

              </div>
            </li>

          </ul>

          <ul class="list-inline menu-left mb-0">
            <li class="float-left">
              <button class="button-menu-mobile open-left waves-light waves-effect">
                <i class="mdi mdi-menu"></i>
              </button>
            </li>
          </ul>

        </nav>

      </div>
      <!-- Top Bar End -->

      <!-- ========== Left Sidebar Start ========== -->

      <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
          <!--- Divider -->
          <div id="sidebar-menu">
            @if (isset($sidebar))
                @include($sidebar)
            @else
                @include('layouts.sidebar.default')
            @endif
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <!-- Left Sidebar End -->

      <!-- ============================================================== -->
      <!-- Start right Content here -->
      <!-- ============================================================== -->
      <div class="content-page">
        <!-- Start content -->
        <div class="content">
          <div class="container-fluid">

            <!-- Breadcrumbs -->
            <div class="row">
              <div class="col-sm-12">
                <div class="page-title-box">
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <!-- Page-Title -->
            @hasSection('title')
            <div class="row">
              <div class="col-sm-12">
                <div class="page-title-box">
                  <h4 class="page-title">@yield('title')</h4>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            @endif

            <div class="row">
              <div class="col-sm-12">
                @yield('content')
              </div>
            </div>

          </div>
          <!-- end container -->
        </div>
        <!-- end content -->

        <footer class="footer">
          2018 © {{ config('app.name', 'Outbreaks') }} - ICNetplc.com
        </footer>

      </div>
      <!-- ============================================================== -->
      <!-- End Right content here -->
      <!-- ============================================================== -->


      <!-- Right Sidebar -->
      <div class="side-bar right-bar">
        <div class="">
          <ul class="nav nav-tabs tabs-bordered nav-justified">
            <li class="nav-item">
              <a href="#home-2" class="nav-link active" data-toggle="tab" aria-expanded="false">
                Activity
              </a>
            </li>
            <li class="nav-item">
              <a href="#messages-2" class="nav-link" data-toggle="tab" aria-expanded="true">
                Settings
              </a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane fade show active" id="home-2">
              <div class="timeline-2">
                <div class="time-item">
                  <div class="item-info">
                    <small class="text-muted">5 minutes ago</small>
                    <p><strong><a href="#" class="text-info">John Doe</a></strong> Uploaded a photo <strong>"DSC000586.jpg"</strong></p>
                  </div>
                </div>

                <div class="time-item">
                  <div class="item-info">
                    <small class="text-muted">30 minutes ago</small>
                    <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                    <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                  </div>
                </div>

                <div class="time-item">
                  <div class="item-info">
                    <small class="text-muted">59 minutes ago</small>
                    <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                    <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                  </div>
                </div>

                <div class="time-item">
                  <div class="item-info">
                    <small class="text-muted">1 hour ago</small>
                    <p><strong><a href="#" class="text-info">John Doe</a></strong>Uploaded 2 new photos</p>
                  </div>
                </div>

                <div class="time-item">
                  <div class="item-info">
                    <small class="text-muted">3 hours ago</small>
                    <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                    <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                  </div>
                </div>

                <div class="time-item">
                  <div class="item-info">
                    <small class="text-muted">5 hours ago</small>
                    <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#" class="text-success">John Doe</a>.</p>
                    <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut tincidunt euismod. "</em></p>
                  </div>
                </div>
              </div>
            </div>


            <div class="tab-pane" id="messages-2">

              <div class="row m-t-20">
                <div class="col-8">
                  <h5 class="m-0 font-15">Notifications</h5>
                  <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                </div>
                <div class="col-4 text-right">
                  <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                </div>
              </div>

              <div class="row m-t-20">
                <div class="col-8">
                  <h5 class="m-0 font-15">API Access</h5>
                  <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                </div>
                <div class="col-4 text-right">
                  <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                </div>
              </div>

              <div class="row m-t-20">
                <div class="col-8">
                  <h5 class="m-0 font-15">Auto Updates</h5>
                  <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                </div>
                <div class="col-4 text-right">
                  <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                </div>
              </div>

              <div class="row m-t-20">
                <div class="col-8">
                  <h5 class="m-0 font-15">Online Status</h5>
                  <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                </div>
                <div class="col-4 text-right">
                  <input type="checkbox" checked data-plugin="switchery" data-color="#3bafda" data-size="small"/>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- /Right-bar -->

    </div>
    <!-- End page -->

    <!-- Scripts -->
    <script src="{{mix('/js/app.js')}}"></script>
    @yield('scripts')
  </body>
</html>
