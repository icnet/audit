<ul>
    <li>
        <a href="{{env('KEYCLOAK_URI')}}/auth/realms/{{env('KEYCLOAK_REALM')}}/account/?referrer={{session('referrer')}}" class="waves-effect waves-primary">
            <i class="ti ti-arrow-left"></i>
            <span>{{__('Account')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('avatar')}}" class="waves-effect waves-primary active">
            <i class="ti ti-image"></i>
            <span>{{__('Avatar')}}</span>
        </a>
    </li>
</ul>
