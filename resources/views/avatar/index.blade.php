@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card card-default">
                    <div class="card-body" id="avatar-panel">
                        <div id="avatar">
                            <div id="avatar-img">
                                @if (isset($avatar))
                                    <img src="{{$avatar}}">
                                @else
                                    <span class="mdi mdi-user" style="padding: 30px; font-size: 200px"></span>
                                    <img>
                                @endif
                            </div>

                            <button class="btn mdi mdi-cloud-upload" id="upload-file" style="font-size: 30px"></button>
                            <button class="btn mdi mdi-camera" id="use-camera" style="font-size: 30px"></button>
                        </div>

                        <div id="selfie-panel" style="display: none">
                            <div id="selfie"></div>
                            <button type="button" class="btn btn-primary" id="take-picture">{{__('Take Picture')}}</button>
                            <button type="button" class="btn btn-success" id="save-picture" style="display: none">{{__('Save Picture')}}</button>
                            <button type="button" class="btn" id="cancel-picture">{{__('Cancel')}}</button>
                            <form action="{{url('/crop')}}" method="post" id="frm-crop">
                                <input type="hidden" name="picture">
                                {{csrf_field()}}
                            </form>
                        </div>

                        <div id="file-upload-panel" style="display: none">
                            <form action="{{url('/upload')}}" method="post" enctype="multipart/form-data" id="frm-upload">
                                {{csrf_field()}}
                                <label class="btn btn-default">
                                    {{__('Choose picture')}} <input type="file" name="file" accept="{{implode(', ', $mimeTypes)}}" hidden>
                                </label>
                                <button type="submit" class="btn btn-primary">{{__('Upload')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    new Avatar();
    </script>
@endsection
