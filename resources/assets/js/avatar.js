/**
 * Handles uploading, webcam capture and cropping of user-avatar images
 *
 * @param {Object} p parameters
 */
var Avatar = function (p) {
    var that = this;

    $.extend(this, p);

    // Hide webcam if not supported
    if (Webcam.userMedia === false) {
        $('#use-camera').hide();
    }

    $('#avatar').on('click', '#use-camera', function () {
        that.webcam();
    });
    $('#avatar').on('click', '#upload-file', function () {
        that.uploadPanel();
    });
    $('#file-upload-panel').on('submit', '#frm-upload', function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        that.fileUpload($(this), formData);
    });
    $('#selfie-panel').on('click', '#take-picture', function () {
        that.take();
    });
    $('#selfie-panel').on('click', '#save-picture', function () {
        that.save();
    });
    $('#selfie-panel').on('click', '#cancel-picture', function () {
        that.cancel();
    });
};
Avatar.prototype = {
    /**
     * @var {Mixed} data URI
     */
    dataUri: null,

    /**
     * @var {Object} crop dimensions
     */
    cropDimensions: {},

    /**
     * Turn on the webcam
     */
    webcam: function () {
        $('#avatar').hide();
        Webcam.set({
            width: 500,
            height: 375,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach('#selfie');
        $('#selfie-panel').show();
    },
    /**
     * Take a picture with the webcam
     */
    take: function () {
        var that = this;
        Webcam.snap(function (dataUri) {
            var img = $('<img/>').attr('src', dataUri);
            that.dataUri = dataUri;
            $('#take-picture').hide();
            $('#save-picture').show();
            $('#selfie').empty();
            $('#selfie').append(img);
            that.cropper(img);
        });
    },
    /**
     * Initialize the cropper on the image
     * @param {Object} img image object
     */
    cropper: function (img) {
        var that = this;
        img.cropper({
            aspectRatio: 1 / 1,
            movable: false,
            rotatable: false,
            scalable: false,
            zoomable: false,
            crop: function (e) {
                that.cropDimensions.x = e.x;
                that.cropDimensions.y = e.y;
                that.cropDimensions.width = e.width;
                that.cropDimensions.height = e.height;
            }
        });
    },
    /**
     * Cancel uploads/cropping etc. and revert back to the start
     */
    cancel: function () {
        this.dataUri = null;
        this.cropDimensions = {};
        $('#selfie-panel').hide();
        $('#selfie-panel #take-picture').show();
        $('#selfie-panel #save-picture').hide();
        $('#avatar').show();
        Webcam.reset();
    },
    /**
     * Save the currently cropped image
     */
    save: function () {
        var that = this,
            rawImageData = this.dataUri.replace(/^data\:image\/\w+\;base64\,/, ''),
            frm = $('#frm-crop');
            frm.find('input[name="picture"]').val(rawImageData);
        $.ajax({
            url: frm.attr('action'),
            method: frm.attr('method'),
            data: frm.serialize() + '&' + $.param(this.cropDimensions),
            dataType: 'json',
            success: function (json) {
                $('#avatar-img span').hide();
                $('#avatar-img img').attr('src', json.src);
                that.cancel();
            }
        });
    },
    /**
     * Exposes the file upload panel
     */
    uploadPanel: function () {
        $('#avatar').hide();
        $('#file-upload-panel').show();
    },
    /**
     * Uploads a new image file
     * @param {Object} frm form object
     * @param {Object} FormData object
     */
    fileUpload: function (frm, formData) {
        var that = this;
        $.ajax({
            url: frm.attr('action'),
            method: frm.attr('method'),
            enctype: frm.attr('enctype'),
            processData: false,
            contentType: false,
            data: formData,
            dataType: 'json',
            success: function (json) {
                var img = $('<img/>').attr('src', json.src);
                that.dataUri = json.src;
                $('#selfie').empty();
                $('#selfie').append(img);
                that.cropper(img);
                $('#file-upload-panel').hide();
                $('#selfie-panel').show();
                $('#selfie-panel #take-picture').hide();
                $('#selfie-panel #save-picture').show();
            }
        });
    }
};

// Export the module
module.exports = Avatar;
