<?php
return [
    'uri' => env('KEYCLOAK_URI'),
    'client_id' => env('KEYCLOAK_URI'),
    'realm' => env('KEYCLOAK_REALM'),
    'secret' => env('KEYCLOAK_SECRET'),
    'redirect_uri' => env('KEYCLOAK_REDIRECT_URI'),
];
