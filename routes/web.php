<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('keycloak/authorize', 'Auth\LoginController@redirect')->name('login');
Route::get('keycloak/redirect', 'Auth\LoginController@callback');

// Default redirect to profile edit
Route::get('/', function () {
    return redirect()->route('avatar');
});

// Avatar editing routes
Route::get('/avatar', 'AvatarController@index')->name('avatar');
Route::post('/upload', 'AvatarController@upload');
Route::post('/crop', 'AvatarController@crop');

// Non-existant images in storage will be replaced with the no_avatar.jpg
Route::get('/storage/{imageNotFound}', function () {
    return response(file_get_contents(__DIR__  . '/../resources/assets/img/no_avatar.jpg'))
        ->header('Content-Type', 'image/jpeg');
});

// Route for getting authenticated users own avatar
Route::get('/my', 'AvatarController@my');

// Logout via keycloak
Route::post('/logout', function () {
    Auth::logout();
    $logoutUrl = env('KEYCLOAK_URI') . "/auth/realms/" . env('KEYCLOAK_REALM') . "/protocol/openid-connect/logout?redirect_uri=" . urlencode(url('/keycloak/authorize'));
    return redirect($logoutUrl);
});
