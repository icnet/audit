<?php

namespace App\Providers\Keycloak;

use Laravel\Socialite\Two\ProviderInterface;
use SocialiteProviders\Manager\OAuth2\AbstractProvider;
use SocialiteProviders\Manager\OAuth2\User;

/**
 * Provider
 *
 * keycloak Socialite Provider add on
 *
 * @author Ian Smith <ian_smith1@baxter.com>
 */
class Provider extends AbstractProvider implements ProviderInterface
{
    /**
     * Unique Provider Identifier.
     * @var string
     */
    const IDENTIFIER = 'keycloak';

    /**
     * {@inheritdoc}
     * @var array
     */
    protected $scopes = ['view-profile', 'manage-account'];

    /**
     * {@inheritdoc}
     * @param  string $state state
     * @return string
     */
    protected function getAuthUrl($state)
    {
        $auth = "/auth/realms/__REALM__/protocol/openid-connect/auth";
        $url = env('KEYCLOAK_URI') . str_replace('__REALM__', env('KEYCLOAK_REALM'), $auth);
        return $this->buildAuthUrlFromBase($url, $state);
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    protected function getTokenUrl()
    {
        $token = "/auth/realms/__REALM__/protocol/openid-connect/token";
        $url = env('KEYCLOAK_URI') . str_replace('__REALM__', env('KEYCLOAK_REALM'), $token);
        return $url;
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    protected function getUserDataUrl()
    {
        $userData = "/auth/realms/__REALM__/protocol/openid-connect/userinfo";
        $url = env('KEYCLOAK_URI') . str_replace('__REALM__', env('KEYCLOAK_REALM'), $userData);
        return $url;
    }

    /**
     * {@inheritdoc}
     * @param string $token token
     * @return string
     */
    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get($this->getUserDataUrl(), [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * {@inheritdoc}
     * @param array $user user as array
     * @return User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User())->setRaw($user)->map([
            'id'         => $user['sub'],
            'name'       => $user['preferred_username'],
            'first_name' => $user['given_name'],
            'last_name'  => $user['family_name'],
            'nickname'   => $user['name'],
            'email'      => $user['email'] ?? '',
        ]);
    }

    /**
     * {@inheritdoc}
     * @param string $code token
     * @return array
     */
    protected function getTokenFields($code)
    {
        return array_merge(parent::getTokenFields($code), [
            'grant_type' => 'authorization_code'
        ]);
    }
}
