<?php

namespace App\Providers\Keycloak;

use SocialiteProviders\Manager\SocialiteWasCalled;

/**
 * KeycloakExtendSocialite
 *
 * Handler to add in keycloak Socialite Provider
 *
 * @author Ian Smith <ian_smith1@baxter.com>
 */
class KeycloakExtendSocialite
{
    /**
     * Execute the provider.
     *
     * @param SocialiteWasCalled $socialiteWasCalled Instance of socialite manager
     *
     * @return void
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('keycloak', Provider::class);
    }
}
