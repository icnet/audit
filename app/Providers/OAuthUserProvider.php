<?php
namespace App\Providers;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\User;

/**
 * KeycloakServiceProvider
 *
 * @author Ian Smith <ian_smith1@baxter.com>
 */
class OAuthUserProvider
{

    /**
     * Creates or retrieves a user object from a given OAuthUser (from keycloak)
     *
     * @param ProviderUser $providerUser Provider user object
     *
     * @return boolean true if valid otherwise false
     */
    public function createOrGetUser(ProviderUser $providerUser)
    {
        // Get role info
        $userAsArray = $providerUser->getRaw();

        // Check to see if we already have an aouth user in
        // the oauth_identities table
        $user = User::whereProvider(Keycloak\Provider::IDENTIFIER)
            ->whereUsername($providerUser->getName())
            ->first();

        if ($user) {
            // Update the user details in case they have changed
            $user->email = $providerUser->getEmail();
            $user->name = $providerUser->getNickname();
            $user->first_name = $userAsArray['given_name'];
            $user->last_name = $userAsArray['family_name'];
            $user->save();
        } else {
            // No user in users table so add one
            $user = User::create([
                'email' => $providerUser->getEmail(),
                'name' => $providerUser->getNickname(),
                'first_name' => $userAsArray['given_name'],
                'last_name' => $userAsArray['family_name'],
                'username' => $providerUser->getName(),
                'provider' => Keycloak\Provider::IDENTIFIER,
                'provider_id' => $providerUser->getId(),
            ]);
            $user->save();
        }
        return $user;
    }
}
