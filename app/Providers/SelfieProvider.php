<?php
namespace App\Providers;

use Illuminate\Filesystem\FilesystemAdapter;

/**
 * Provider for maintaining the user selfie
 */
class SelfieProvider
{
    /**
     * @var int destination image width/height
     */
    protected $destinationSize = 300;

    /**
     * @var string username
     */
    protected $username;

    /**
     * @var string realm
     */
    protected $realm;

    /**
     * @var FilesystemAdapter storage
     */
    protected $storage;

    /**
     * @var array of allowable mime types
     */
    protected $mimeTypes = [
        'image/jpeg',
        'image/png',
        'image/gif',
    ];

    /**
     * Saves and crops the given selfie/upload
     *
     * @param mixed $selfie raw image (base64 encoded) or file path
     * @param int   $x      x crop co-ordinate
     * @param int   $y      y crop co-ordinate
     * @param int   $width  crop width
     * @param int   $height crop height
     * @return boolean
     */
    public function crop($selfie, $x = null, $y = null, $width = null, $height = null): bool
    {
        $source = imagecreatefromstring($selfie);
        $destination = imagecreatetruecolor($this->destinationSize, $this->destinationSize);

        imagecopyresampled(
            $destination,
            $source,
            0,
            0,
            $x,
            $y,
            $this->destinationSize,
            $this->destinationSize,
            $width,
            $height
        );

        ob_start();
        imagejpeg($destination);
        $jpeg = ob_get_clean();

        $this->storage->put(
            sprintf('%s.jpg', $this->hash()),
            $jpeg
        );

        return true;
    }

    /**
     * Returns the list of allowable mime types for file upload
     *
     * @return array
     */
    public function mimeTypes()
    {
        return $this->mimeTypes;
    }

    /**
     * Sets the username
     *
     * @param string $username username
     * @return SelfieProvider
     */
    public function setUsername($username): SelfieProvider
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Sets the realm
     *
     * @param string $realm realm
     * @return SelfieProvider
     */
    public function setRealm($realm): SelfieProvider
    {
        $this->realm = $realm;
        return $this;
    }

    /**
     * Sets the storage
     *
     * @param FilesytemAdapter $storage storage
     * @return SelfieProvider
     */
    public function setStorage(FilesystemAdapter $storage): SelfieProvider
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * Returns the hash of the username
     *
     * @return string
     * @throws \Exception
     */
    public function hash(): string
    {
        if (!isset($this->username)) {
            throw new \Exception('Username not set');
        }
        if (!isset($this->realm)) {
            throw new \Exception('Realm not set');
        }
        return md5(sprintf('%s.%s', $this->realm, $this->username));
    }
}
