<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\Keycloak\Provider;
use App\Providers\OAuthUserProvider;
use Socialite;

/**
 * Login Controller
 */
class LoginController extends Controller
{
    /**
     * Create new controller instance
     */
    public function __construct()
    {
        // Disable authentication otherwise endless auth loop
        $this->middleware('guest');
    }

    /**
     * Redirect the user to the keycloak authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        return Socialite::with(Provider::IDENTIFIER)->stateless()->redirect();
    }

    /**
     * Obtain the user information from Keycloak
     *
     * @param OAuthUserProvider $provider OAuthUser provider
     * @return \Illuminate\Http\Response
     */
    public function callback(OAuthUserProvider $provider)
    {
        $user = $provider->createOrGetUser(
            Socialite::driver(Provider::IDENTIFIER)->stateless()->user()
        );

        auth()->login($user);

        return redirect()->to('/');
    }
}
