<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\SelfieProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * Avatar controller
 */
class AvatarController extends Controller
{
    /**
     * Main index
     *
     * @param SelfieProvider $selfie  instance of the selfie provider
     * @param Request        $request HTTP request
     * @return string HTML
     */
    public function index(SelfieProvider $selfie, Request $request)
    {
        // If this has been called from keycloak the referrer client-id will be included in the request.
        // This should be written to the current session as it is used in the back navigation.
        if ($request->exists('referrer')) {
            session([ 'referrer' => $request->input('referrer') ]);
        }

        $selfie->setRealm(env('KEYCLOAK_REALM'))
            ->setUsername(Auth::user()->username);

        $avatar = sprintf('%s.jpg', $selfie->hash());

        $view = [
            'mimeTypes' => $selfie->mimeTypes(),
        ];
        if (Storage::disk('public')->exists($avatar)) {
            $view['avatar'] = asset(sprintf('storage/%s?%s', $avatar, uniqid()));
        }

        return view('avatar.index', $view);
    }

    /**
     * Action which returns an authenticated users own avatar
     *
     * @param SelfieProvider $selfie instance of the selfie provider
     * @return Illuminate\Http\RedirectResponse
     */
    public function my(SelfieProvider $selfie)
    {
        $selfie->setRealm(env('KEYCLOAK_REALM'))
            ->setUsername(Auth::user()->username);

        $avatar = sprintf('%s.jpg', $selfie->hash());

        if (Storage::disk('public')->exists($avatar)) {
            return redirect(asset(sprintf('storage/%s?%s', $avatar, uniqid())));
        } else {
            return redirect(asset('storage/no_avatar.jpg'));
        }
    }

    /**
     * Action on receiving a crop request
     *
     * @param Request        $request request
     * @param SelfieProvider $selfie  instance of the selfie provider
     * @return string JSON
     */
    public function crop(Request $request, SelfieProvider $selfie)
    {
        $pictureUri = $request->get('picture');
        if (filter_var($pictureUri, FILTER_VALIDATE_URL)) {
            $validExtensions = str_replace('image/', '', implode('|', $selfie->mimeTypes()));
            if (preg_match('/tmp\/\w+\.(' . $validExtensions . ')$/', $pictureUri, $matches)) {
                if (Storage::disk('public')->exists($matches[0])) {
                    $picture = Storage::disk('public')->get($matches[0]);
                    Storage::disk('public')->delete($matches[0]);
                }
            }
        } else {
            $picture = base64_decode($pictureUri);
        }

        if (isset($picture)) {
            $selfie->setRealm(env('KEYCLOAK_REALM'))
                ->setUsername(Auth::user()->username)
                ->setStorage(Storage::disk('public'))
                ->crop(
                    $picture,
                    $request->get('x'),
                    $request->get('y'),
                    $request->get('width'),
                    $request->get('height')
                );
            return json_encode([
                'src' => asset(sprintf('storage/%s.jpg?%s', $selfie->hash(), uniqid())),
            ]);
        } else {
            throw new \Exception('Picture to crop not found');
        }
    }

    /**
     * User-supplied uploaded file
     *
     * @param Request        $request request
     * @param SelfieProvider $selfie  instance of the selfie provider
     * @return string JSON
     * @throws \InvalidArgumentException
     */
    public function upload(Request $request, SelfieProvider $selfie)
    {
        $file = $request->file('file');
        if (!in_array($file->getClientMimeType(), $selfie->mimeTypes())) {
            throw new \InvalidArgumentException('MIME type invalid');
        }

        $path = $file->store('tmp', 'public');

        return json_encode([
            'src' => asset(sprintf('storage/%s', $path)),
        ]);
    }
}
