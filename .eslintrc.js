module.exports = {
    "env": {
        "browser": true,
        "jquery": true
    },
    "globals": {
        "Modernizr": true
    },
    "extends": "eslint:recommended",
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
